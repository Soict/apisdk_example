import UIKit
import CoreSDK
import SVPullToRefresh

class PostController: UIViewController {

    @IBOutlet weak var tbvList: UITableView!
    
    private var ids: [Int] = []
    private var posts: [PostData] = []
    
    //API Service
    private var service: PostService = PostService()
    private var offset: Int = 0
    private var limit: Int = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        self.loadData()
    }

    private func setupUI() {
        self.tbvList.register(PostContentCell.nib, forCellReuseIdentifier: PostContentCell.identifier)
        
        self.tbvList.addInfiniteScrolling {
            self.loadPostsWithComment(isLoadMore: true)
        }
        self.tbvList.addPullToRefresh {
            self.resetPage()
            self.loadData(isRefresh: true)
        }
    }
    
    private func loadData(isRefresh: Bool = false) {
        if !isRefresh {
            Utilities.showHUD(self.view)
        }
        self.service.getAllPosts { [weak self] posts, error in
            Utilities.performOnMainThread {
                guard let wSelf = self else { return }
                Utilities.hideHUD(wSelf.view)
                wSelf.ids = posts.map({ $0.getId() ?? 0 }).filter({ $0 != 0 })
                wSelf.loadPostsWithComment(isRefresh: isRefresh)
            }
        }
    }
    
    private func loadPostsWithComment(isLoadMore: Bool = false, isRefresh: Bool = false) {
        if offset >= self.ids.count {
            //Full data
            self.stopAnimating()
            self.tbvList.infiniteScrollingView?.enabled = false
            return
        }
        let available = min(offset + limit, self.ids.count)
        let ids = Array(self.ids[offset..<available])
        if !isLoadMore && !isRefresh {
            Utilities.showHUD(self.view)
        }
        self.service.getPostWithCommentByIds(ids: ids) { [weak self] (posts, error) in
            Utilities.performOnMainThread {
                guard let wSelf = self else { return }
                Utilities.hideHUD(wSelf.view)
                wSelf.stopAnimating()
                if isRefresh {
                    wSelf.posts.removeAll()
                }
                wSelf.posts.append(contentsOf: posts)
                wSelf.tbvList.reloadData()
                wSelf.nextPage()
            }
        }
    }
    
    private func resetPage() {
        self.offset = 0
        self.tbvList.infiniteScrollingView?.enabled = true
    }
    
    private func nextPage() {
        self.offset += self.limit
    }
    
    private func stopAnimating() {
        self.tbvList.infiniteScrollingView?.stopAnimating()
        self.tbvList.pullToRefreshView?.stopAnimating()
    }
}

extension PostController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PostContentCell.identifier) as? PostContentCell else { return UITableViewCell() }
        cell.selectionStyle = .none
        
        if indexPath.count < self.posts.count {
            cell.updateData(self.posts[indexPath.row])
        }
        
        return cell
    }
}

extension PostController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return PostContentCell.height
    }
}
