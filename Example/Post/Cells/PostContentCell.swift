import UIKit
import CoreSDK

class PostContentCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBody: UILabel!
    @IBOutlet weak var lblCommentCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.mainView.layer.borderWidth = 1.0
        self.mainView.layer.borderColor = UIColor.lightGray.cgColor
        self.mainView.layer.cornerRadius = 10.0
        self.mainView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func updateData(_ post: PostData) {
        self.lblTitle.text = post.getTitle()
        self.lblBody.text = post.getBody()
        let count = post.getComments().count
        self.lblCommentCount.text = "\(count) \(count == 1 ? "comment" : "comments")"
    }
    
    static var nib: UINib {
        return UINib(nibName: PostContentCell.identifier, bundle: nil)
    }
    
    static var identifier: String {
        return "PostContentCell"
    }
    
    static var height: CGFloat {
        return UITableView.automaticDimension
    }
}
