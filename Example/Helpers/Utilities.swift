import UIKit
import MBProgressHUD

class Utilities: NSObject {

    static func performOnMainThread(_ closure: @escaping () -> Void) {
        if Thread.isMainThread {
            closure()
        } else {
            DispatchQueue.main.async {
                closure()
            }
        }
    }
    
}

//MARK: - Show/Hide loading -
extension Utilities {
    static func showHUD(_ topView: UIView? = nil, _ animated: Bool = true) {
        var root = topView
        if topView == nil {
            if let window = Utilities.getWindow() {
                root = window
            }
        }
        guard let window = root else {
            return
        }
        //Show/Hide loading
        let hud = MBProgressHUD.showAdded(to: window, animated: animated)
        hud.bezelView.color = UIColor.clear
        hud.bezelView.style = .solidColor
        hud.show(animated: animated)
    }
    
    static func hideHUD(_ topView: UIView? = nil, _ animated: Bool = true) {
        var root = topView
        if topView == nil {
            root = Utilities.getWindow()
        }
        guard let window = root else {
            return
        }
        MBProgressHUD.hide(for: window, animated: animated)
    }
    
    static private func getWindow() -> UIView? {
        if #available(iOS 15.0, *) {
            let scenes = UIApplication.shared.connectedScenes.first as? UIWindowScene
            return scenes?.windows.last
         } else {
             return UIApplication.shared.windows.last
          }
    }
}
